package pl.codementors.comics.model;

import java.util.Objects;

/**
 * Class for Pair.
 * @param <T> Left element of the pair.
 * @param <V> Right element of the pair.
 *
 * @author Paweł Wieraszka
 */
public class Pair<T, V> {

        private T left;

        private V right;

        public T getLeft() {
            return left;
        }

        public void setLeft(T left) {
            this.left = left;
        }

        public V getRight() {
            return right;
        }

        public void setRight(V right) {
            this.right = right;
        }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(left, pair.left) &&
                Objects.equals(right, pair.right);
    }

    @Override
    public int hashCode() {

        return Objects.hash(left, right);
    }
}

