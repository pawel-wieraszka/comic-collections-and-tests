package pl.codementors.comics;

import pl.codementors.comics.model.Pair;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Represents comics library. Contains inner collection with comics, exposes methods for adding and removing comics.
 * Allows for changing covers of all contained comics.
 * Almost all comments were written by psysiu. All methods and tests for them were written by Paweł Wieraszka
 * according to comments.
 *
 * @author psysiu & Paweł Wieraszka
 */
public class ComicsLibrary {

    private static final Logger log = Logger.getLogger(ComicsLibrary.class.getCanonicalName());

    /**
     * Set of comics contained in the library.
     */
    private Set<Comic> comics = new HashSet<>();

    /**
     *
     * @return All comics in the library. The returned collection is unmodifiable so it can not be changed
     * outside the library.
     */
    public Collection<Comic> getComics() {
        return comics;
    }

    /**
     * Adds comic to the library. If comic is already in the library does nothing. If comic is null does nothing.
     *
     * @param comic Comic to be added to the library.
     */
    public void add(Comic comic) {
        if (comic != null && !comics.contains(comic)) {
            comics.add(comic);
        }
    }

    /**
     * Removes comic from the library. If comics is not present in the library does nothing.
     *
     * @param comic Comic to be removed from the library.
     */
    public void remove(Comic comic) {
        if (comics.contains(comic))
            comics.remove(comic);
    }

    /**
     * Changes covers of all comics in the library.
     *
     * @param cover Cover type for all comics in the library.
     */
    public void changeCovers(Comic.Cover cover) {
            for (Comic c : comics)
                c.setCover(cover);
    }

    /**
     *
     * @return All authors of all comics in the library. Each author is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public Collection<String> getAuthors() {
        Set<String> authors = new HashSet<>();
        for (Comic c : comics)
            authors.add(c.getAuthor());
        return Collections.unmodifiableSet(authors);
    }

    /**
     *
     * @return All series of all comics in the library. Each series is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public Collection<String> getSeries() {
        Set<String> series = new HashSet<>();
        for (Comic c : comics)
            series.add(c.getSeries());
        return Collections.unmodifiableSet(series);
    }

    /**
     * Loads comics from file. Method uses FileReader and Scanner for reading comics from file.
     *
     * The file structure is:
     * number_of_comics (one line with one number, nextInt())
     * comics title (one line with spaces, nextLine())
     * comics author (one line with spaces, nextLine())
     * comics series (one line with spaces, nextLine())
     * cover (one line with one word, next())
     * publish_month (one line with one number, nextInt())
     * publish_year (one line with one number, nextInt())
     *
     * The proper sequence for reading file is to call nextInt(); skip("\n"); to read number of comics.
     * Then in loop call nextLine(); nextLine(); nextLine(), next(); nextInt(); nextInt(); skip("\n").
     *
     * If file does not exists, or is directory, or can not be read, method just ignores it and does nothing.
     *
     * @param file File from which comics will be loaded.
     */
    public void load(File file) {
        if (!file.exists() || file.isDirectory() || !file.canRead()) { }
        else {
            try (Scanner in = new Scanner(new FileReader(file))) {
                if (in.hasNextInt()) {
                    int number = in.nextInt();
                    in.skip("\n");
                    for (int i = 0; i < number; i++) {
                        String title = in.nextLine();
                        String author = in.nextLine();
                        String series = in.nextLine();
                        String coverText = in.next();
                        Comic.Cover cover = Comic.Cover.valueOf(coverText);
                        int publishMonth = in.nextInt();
                        int publishYear = in.nextInt();
                        in.skip("\n");
                        comics.add(new Comic(title, author, series, cover, publishYear, publishMonth));
                    }
                }
            } catch (FileNotFoundException ex1) {
                log.log(Level.SEVERE, ex1.getMessage(), ex1);
            }
        }
    }

    /**
     * Counts all comics with the same provided series name.
     *
     * @param series Name of the series for which comics will be counted.
     * @return Number of comics from the same provided series.
     */
    public int countBySeries(String series) {
        int countSeries = 0;
        for (Comic a : comics)
            if ((a.getSeries()).equals(series))
                countSeries++;
        return countSeries;
    }

    /**
     * Counts all comics with the same provided author.
     *
     * @param author Author for which comics will be counted.
     * @return Number of comics with the same author.
     */
    public int countByAuthor(String author) {
        int countAuthor = 0;
        for (Comic a : comics)
            if ((a.getAuthor()).equals(author))
                countAuthor++;
        return countAuthor;
    }

    /**
     * Counts all comics with the same provided publish hear.
     *
     * @param year Publish year for which comics will be counted.
     * @return Number of comics from the same provided publish year.
     */
    public int countByYear(int year) {
        int countYear = 0;
        for (Comic a : comics)
            if (a.getPublishYear() == year)
                countYear++;
        return countYear;
    }

    /**
     * Counts all comics with the same provided publish hear and month.
     *
     * @param year Publish year for which comics will be counted.
     * @param month Publish mnt for which comics will be counted.
     * @return Number of comics from the same provided publish year and month.
     */
    public int countByYearAndMonth(int year, int month) {
        int countYearMonth = 0;
        for (Comic a : comics)
            if (a.getPublishYear() == year && a.getPublishMonth() == month)
                countYearMonth++;
        return countYearMonth;
    }

    /**
     * Removes all comics with publish year smaller than the provided year. For the removal process
     * method uses iterator.
     *
     * @param year Provided yer.
     */
    public void removeAllOlderThan(int year) {
        Iterator<Comic> it = comics.iterator();
        while (it.hasNext()) {
            Comic c = it.next();
            if (c.getPublishYear() < year)
                it.remove();
        }
    }

    /**
     * Removes all comics written by the specified author. For the removal process method uses iterator.
     *
     * @param author Provided author.
     */
    public void removeAllFromAuthor(String author) {
        Iterator<Comic> it = comics.iterator();
        while (it.hasNext()) {
            Comic c = it.next();
            if ((c.getAuthor()).equals(author))
                it.remove();
        }
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping author->comics. Map keys are names of the authors (String) present in the library. Map values are
     * collection (e.g.: HashSet<Comic>) of comics for specified author.
     */
    public Map<String, Collection<Comic>> getAuthorsComics() {
        Map<String, Collection<Comic>> authorsMap = new HashMap<>();
        for (Comic c : comics) {
            if (!authorsMap.containsKey(c.getAuthor())) {
                authorsMap.put(c.getAuthor(), new HashSet<>());
            }
            authorsMap.get(c.getAuthor()).add(c);
        }
        return authorsMap;
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping publish year->comics. Map keys are publish year (Integer, generics can not be simple types
     * so instead int the Integer is used) present in the library. Map values are collection (e.g.: HashSet<Comic>)
     * of comics for specified year.
     */
    public Map<Integer, Collection<Comic>> getYearsComics() {
        Map<Integer, Collection<Comic>> yearsMap = new HashMap<>();
        for (Comic c : comics) {
            if (!yearsMap.containsKey(c.getPublishYear())) {
                yearsMap.put(c.getPublishYear(), new HashSet<>());
            }
            yearsMap.get(c.getPublishYear()).add(c);
        }
        return yearsMap;
    }

    //Zadanie 5
    /**
     * Removes all comics with publish year higher than the provided year. Method uses iterator for the removal process.
     *
     * @param year Provided year.
     */
    public void removeAllNewerThan(int year) {
        Iterator<Comic> it = comics.iterator();
        while (it.hasNext()) {
            Comic c = it.next();
            if (c.getPublishYear() > year)
                it.remove();
        }
    }

    //Zadanie 6
    /**
     * Removes all comics with publish year and month equals to provided year and month.
     *
     * @param year Provided year.
     * @param month Provided month.
     */
    public void removeAllFromYearAndMonth(int year, int month) {
        Iterator<Comic> it = comics.iterator();
        while (it.hasNext()) {
            Comic c = it.next();
            if (c.getPublishYear() == year && c.getPublishMonth() == month)
                it.remove();
        }
    }

    //Zadanie 7
    /**
     * Creates map and returns it.
     *
     * @return Mapping publish year and month -> comics. Map keys are publish year and month present in the library.
     * Map values are collection of comics for specified year and month.
     */
    public Map<Pair<Integer, Integer>, Collection<Comic>> getYearsMonthsComics() {
        Map<Pair<Integer, Integer>, Collection<Comic>> yearsMonthsMap = new HashMap<>();
        for (Comic c : comics) {
            Pair<Integer, Integer> comicsPair = new Pair<>();
            comicsPair.setLeft(c.getPublishYear());
            comicsPair.setRight(c.getPublishMonth());
            if (!yearsMonthsMap.containsKey(comicsPair)) {
                yearsMonthsMap.put(comicsPair, new HashSet<>());
            }
            yearsMonthsMap.get(comicsPair).add(c);
        }
        return yearsMonthsMap;
    }
}
